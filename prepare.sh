#!/bin/bash
set -e
set -u

pacman -Syu --noconfirm 
pacman --needed -S base-devel git pacman-contrib curl jq --noconfirm 

groupadd user
useradd -m -g user user
