#!/bin/bash
set -e
set -u

echo "install aur package: $1"
pacman -Qi $1 > /dev/null && exit #check if package is already installed
mkdir -p /tmp/aur
cd /tmp/aur
packagebase=$(curl -s "https://aur.archlinux.org/rpc/?v=5&type=info&arg[]=$1" | jq '.results[] | .PackageBase')
packagebase=${packagebase:1:-1}
test -z "$packagebase" && packagebase="$1"
git clone "https://aur.archlinux.org/$packagebase.git"
cd "$packagebase"

source ./PKGBUILD
officially_packages=""
aur_packages=""
dependency=("${makedepends[@]}" "${depends[@]}")
for package in "${dependency[@]}"
do
	package=$( echo "$package" | xargs) 
	package=$(echo "$package" | cut -f1 -d">" | cut -f1 -d"<" | cut -f1 -d"=")
	if ! pacman -Qi $package > /dev/null #check if package is already installed
	then
		if pacman -Ss ^$package$ > /dev/null #chen if package is officiall or a aur package
		then
			echo "need officially package #$package#"
			officially_packages+=" $package"
		else
			echo "need aur package #$package#"
			aur_packages+=" $package"
		fi
	fi	
done
test -z "$officially_packages" || pacman --asdeps --needed --noconfirm -S $officially_packages
aur_packages=($aur_packages)
for package in "${aur_packages[@]}"
do
	$CI_BUILDS_DIR/$CI_PROJECT_PATH/build-aur.sh "$package" --asdeps
done

chown -R user .
su user -c "makepkg -c"
pacman --needed --noconfirm ${2:-} -U *.tar.zst
